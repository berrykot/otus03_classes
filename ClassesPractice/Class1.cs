﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassesPractice
{
	class Class1
	{
		public void Show()
		{
			A a = new A(), b = new B(), c = new C();
			I ia = new A(), ib = new B(), ic = new C();
			Console.WriteLine($"{a.P}, {b.P}, {c.P}, {ia.P}, {ib.P}, {ic.P}");//"0, 1, 1, 0, 1, 2"
		}
		interface I
		{
			int P { get; }
		}
		class A : I
		{
			public virtual int P => 0;
		}
		class B : A
		{
			public override int P => 1;
		}
		class C : B, I
		{
			public int P => 2;
		}
	}
}
