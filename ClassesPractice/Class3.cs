﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassesPractice
{
	class Class3
	{
		public void Show()
		{
			A a = new A();
			a.Foo();//A

			A b2 = new B();
			b2.Foo();//B

			A c = new C();
			c.Foo();//A

			D d = new D();
			d.Foo();//D

			E e = new E();
			e.Foo();//E
		}
		class A
		{
			public virtual void Foo() => Console.WriteLine("A");
		}
		class B : A
		{
			public override void Foo() => Console.WriteLine("B");
		}
		class C : A
		{
			public new void Foo() => Console.WriteLine("C");
		}
		class D : A
		{
			public void Foo() => Console.WriteLine("D");
		}
		class E : B
		{
			public new void Foo() => Console.WriteLine("E");
		}

	}
}
