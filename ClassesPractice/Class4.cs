﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassesPractice
{
	class Class4
	{
		public void Show()
		{
			A a = new B();
			A a1 = new C();
			Console.WriteLine(a.Calc() + a1.Calc());
			var x = a1.Calc();
			//20 * 20 + 30 * 20 = 400 + 600 = 1000
		}

		class A
		{
			public virtual int Calc() => 10 * Gen();
			protected int Gen() => 10;
		}
		class B : A
		{
			public override int Calc() => 20 * Gen();
			protected int Gen() => 20;
		}
		class C : B
		{
			public override int Calc() => 30 * Gen();
			protected int Gen() => base.Gen();
		}
	}
}
