﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassesPractice
{
	class Class2
	{
		public void Show()
		{
			A a = new B();
			//Static constructor B called
			//Constructor A called
			//B value = 0
			//Constructor B called
		}
		public class A
		{
			public A()
			{
				Console.WriteLine("Constructor A called");
				Console.WriteLine($"B value = {B.value}");
			}
		}
		public class B : A
		{
			public B() => Console.WriteLine("Constructor B called");
			static B()
			{
				//Console.WriteLine($"B value = {B.value}");//1
				value = 0;
				//Console.WriteLine($"B value = {B.value}");//0
				Console.WriteLine("Static constructor B called");
			}
			public static int value = 1;
		}
		
	}
}
